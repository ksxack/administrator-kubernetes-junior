## Lens

https://k8slens.dev/

https://github.com/MuhammedKalkan/OpenLens

#### Ubuntu

```sh
wget https://github.com/MuhammedKalkan/OpenLens/releases/download/v6.2.5/OpenLens-6.2.5.amd64.deb

sudo dpkg -i OpenLens-6.2.5.amd64.deb

rm OpenLens-6.2.5.amd64.deb
```


#### Mac OS

```sh
brew install --cask openlens
```