## ktx

```sh
kubectl config get-contexts
kubectl config use-context prod
kubectl config use-context k3s
```

https://github.com/blendle/kns

```sh
sudo curl https://raw.githubusercontent.com/blendle/kns/master/bin/ktx -o /usr/local/bin/ktx && sudo chmod +x $_
```