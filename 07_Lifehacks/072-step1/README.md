## kns

```sh
kubectl -n playground get po
```

https://github.com/blendle/kns

#### Ubuntu

```sh
sudo apt install fzf
```
```sh
sudo curl https://raw.githubusercontent.com/blendle/kns/master/bin/kns -o /usr/local/bin/kns && sudo  chmod +x $_
```


#### Mac OS

```sh
brew install fzf
```
```sh
sudo curl https://raw.githubusercontent.com/blendle/kns/master/bin/kns -o /usr/local/bin/kns && sudo  chmod +x $_
```

#### Usage

```sh
kns
kubectl get po
```