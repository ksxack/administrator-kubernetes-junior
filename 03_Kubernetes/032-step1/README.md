## Namespace

```bash 
kubectl
kubectl get node
kubectl apply -f kubernetes/namespace.yaml 
kubectl get namespaces
kubectl apply -f namespace2.yaml 
kubectl get ns
kubectl delete -f namespace2.yaml 
kubectl get ns
```
