# Get yaml

```bash
kubectl -n default get po
kubectl get pod nginx -o yaml
kubectl get pod nginx -o yaml > nginx.yaml
```