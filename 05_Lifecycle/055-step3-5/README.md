# Ingress Nginx installation

```bash
kubectl apply -f https://raw.githubusercontent.com/kubernetes/ingress-nginx/controller-v1.8.2/deploy/static/provider/cloud/deploy.yaml
```

# hosts

```
192.168.10.1 playground.akj.ru
192.168.10.1 docs.akj.ru
```

# Links 

- https://gist.github.com/grigorkh/f8e4fd73e99f0fde06a51e2ed7c2156c
- https://kubernetes.github.io/ingress-nginx/deploy/
- https://docs.nginx.com/nginx-ingress-controller/installation/installing-nic/installation-with-helm/
