<!-- _coverpage.md -->

# Golang <small>3.5</small>

> Go documentation

- Simple and lightweight

[GitHub](https://github.com/golang/go)
[Get Started](/golang/README.md)